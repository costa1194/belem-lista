package br.com.idevnorte.belemlista;


import android.app.ProgressDialog;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import br.com.idevnorte.belemlista.data.BelemContract;


public class MainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private GridView gridView;
    private AdapterCategoria adapter;
    private static final int CATEGORIA_LOADER = 0;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        gridView = view.findViewById(R.id.lista_categoria);


        adapter = new AdapterCategoria(getContext(), null);

        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Uri uri = BelemContract.CategoriaEntry.buildUriForCategoria(id);

              Callback callback = (Callback) getActivity();
              callback.onItemSelected(uri);
            }
        });

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getString(R.string.pd_carregando_titulo));
        progressDialog.setMessage(getString(R.string.pd_carregando_mensagem));
        progressDialog.setCancelable(false);

        getLoaderManager().initLoader(CATEGORIA_LOADER, null, this);

        new Tread(getContext()).Categoria(adapter);

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        progressDialog.show();

        String[] projection = {
                BelemContract.CategoriaEntry._ID,
                BelemContract.CategoriaEntry.COLUMN_CATEGORIA,
                BelemContract.CategoriaEntry.COLUMN_ICONE
        };

        return new CursorLoader(getContext(), BelemContract.CategoriaEntry.CONTENT_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        adapter.swapCursor(data);
        progressDialog.dismiss();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        adapter.swapCursor(null);
    }


    public interface Callback {
        void onItemSelected(Uri uri);
    }

}
