package br.com.idevnorte.belemlista.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Miguel on 12/11/2017.
 */

public final class BelemContract {

    public static final String CONTENT_AUTHORITY = "br.com.idevnorte.belemlista";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_CATEGORIA = "Categoria";
    public static final String PATH_EMPRESA = "Empresa";

    public static abstract class CategoriaEntry implements BaseColumns{

        public static  final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CATEGORIA).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORIA;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORIA;


        public static final String TABLE_NAME = "Categoria";

        public static final String _ID = "_id";
        public static final String COLUMN_CATEGORIA = "categoria";
        public static final String COLUMN_ICONE = "icone";

        public static Uri buildUriForCategoria() {
            return CONTENT_URI.buildUpon().build();
        }

        public static Uri buildUriForCategoria(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static long getIdFromUri (Uri uri){
            return Long.parseLong(uri.getPathSegments().get(1));
        }
    }




    public static abstract class EmpresaEntry implements BaseColumns{

        public static  final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_EMPRESA).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EMPRESA;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_EMPRESA ;


        public static final String TABLE_NAME = "Empresa";

        public static final String _ID = "_id";
        public static final String COLUMN_EMPRESA = "empresa";
        public static final String COLUMN_ENDERECO = "endereco";
        public static final String COLUMN_ICONE = "icone";
        public static final String COLUMN_CATEGORIA__ID = "Categoria__id";
        public static final String COLUMN_TAG = "tag";

        public static Uri buildUriForEmpresa() {
            return CONTENT_URI.buildUpon().build();
        }

        public static Uri buildUriForEmpresa(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static long getIdFromUri (Uri uri){
            return Long.parseLong(uri.getPathSegments().get(1));
        }
    }


}
