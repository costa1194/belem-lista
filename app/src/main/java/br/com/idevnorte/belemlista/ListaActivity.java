package br.com.idevnorte.belemlista;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class ListaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        Uri uri = intent.getData();


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);





        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        ListFragment fragment = new ListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(MainActivity.KEY_CATEGORIA, uri);
        fragment.setArguments(bundle);

        transaction.add(R.id.list_fragment, fragment);

        transaction.commit();

    }
}
