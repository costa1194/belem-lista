package br.com.idevnorte.belemlista;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import br.com.idevnorte.belemlista.data.BelemContract;


public class ListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView listView;
    private Uri categoriaUri;
    private AdapterEmpresa adapter;
    private static final int EMPRESA_LOADER = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoriaUri = getArguments().getParcelable(MainActivity.KEY_CATEGORIA);
            Log.d(MainActivity.KEY_CATEGORIA + " Recebida", String.valueOf(categoriaUri));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        listView = view.findViewById(R.id.lista_empresa);

        adapter = new AdapterEmpresa(getContext(), null);

        listView.setAdapter(adapter);

        getLoaderManager().initLoader(EMPRESA_LOADER, null, this);

        new Tread(getContext()).Empresa(adapter, categoriaUri);


        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = {
                BelemContract.EmpresaEntry._ID,
                BelemContract.EmpresaEntry.COLUMN_EMPRESA,
                BelemContract.EmpresaEntry.COLUMN_ENDERECO,
                BelemContract.EmpresaEntry.COLUMN_TAG,
                BelemContract.EmpresaEntry.COLUMN_ICONE,
                BelemContract.EmpresaEntry.COLUMN_CATEGORIA__ID
        };

        return new CursorLoader(getContext(), BelemContract.EmpresaEntry.CONTENT_URI, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        adapter.swapCursor(data);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
