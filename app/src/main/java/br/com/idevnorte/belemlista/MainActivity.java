package br.com.idevnorte.belemlista;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements MainFragment.Callback{

    public static final String KEY_CATEGORIA ="Categoria";
    private boolean isTablet = false;

    public MainActivity() throws IOException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void onItemSelected(Uri uri) {
        if(isTablet){

        }else{
            Intent intent = new Intent(this, ListaActivity.class);
            intent.setData(uri);
            startActivity(intent);
        }
    }
}
