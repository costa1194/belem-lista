package br.com.idevnorte.belemlista;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Item Categoria: recebe todas as informações da Categoria
 *
 * Created by Miguel on 24/10/2017.
 */

public class ItemCategoria implements Serializable {

    private long id;

    private String titulo;

    private String icone;

    /**
     * Constutor do ItemCategoria que vai receber as informções obrigatorias para se criar uma categoria.
     *
     * @param id
     * @param titulo : Nome da Categoria
     * @param icone : Nome do Icone
     */
    public ItemCategoria(Long id, String titulo, String icone) {
        this.id = id;
        this.titulo = titulo;
        this.icone = icone;

    }

    /**
     * Construtor do ItemCategoria que vai receber o arquivo Json da API.
     *
     * @param json : Objeto em formato Json que contem todos os parametros de uma Categoria
     *
     * @throws JSONException : Erro caso o arquivo Json não possa ser lido.
     */
    public ItemCategoria(JSONObject json) throws JSONException {
        this.id = json.getLong("id");
        this.titulo = json.getString("Categoria");
        this.icone = json.getString("icone");
    }



    /** Getter e Setter */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }
}
