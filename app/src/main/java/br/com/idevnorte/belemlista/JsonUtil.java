package br.com.idevnorte.belemlista;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Classe que trata os arquivo Json
 *
 */

public class JsonUtil {

    /**
     * Metodo responsavel por tratar o arquivo Json para uma Categoria.
     *
     * @param json : Arquivo Json.
     *
     * @return : Lista de ItemCategoria populada com as informações contidas no arquivo Json
     */
    public static ArrayList<ItemCategoria> categoriaJson(String json) {

        ArrayList<ItemCategoria> list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(json);
            JSONArray categorias = object.getJSONArray("Categarias");

            /*Adicionar os objetos Jsons linha a linha na lista*/
            for (int i = 0; i < categorias.length(); i++) {

                JSONObject categoriaObject = categorias.getJSONObject(i);
                ItemCategoria itemCategoria = new ItemCategoria(categoriaObject);
                Log.d("Categoria", String.valueOf(itemCategoria.getId()));
                list.add(itemCategoria);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
    /**
     * Metodo responsavel por tratar o arquivo Json para uma Empresa.
     *
     * @param json : Arquivo Json.
     *
     * @return : Lista de ItemEmpresa populada com as informações contidas no arquivo Json
     */
    public static ArrayList<ItemEmpresa> empresaJson(String json, ItemCategoria categoria) {

        ArrayList<ItemEmpresa> list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(json);
            JSONArray empresas = object.getJSONArray("Empresas");

             /*Adicionar os objetos Jsons linha a linha na lista SE o objeto for da categoria escolhida pelo visitante*/
            for (int i = 0; i < empresas.length(); i++) {

                JSONObject empresaObject = empresas.getJSONObject(i);
                long id = empresaObject.getLong("Categoria");
categoria = new ItemCategoria(1l,"ola","farmacia");
                if ( id == categoria.getId()) {
                    ItemEmpresa itemEmpresa = new ItemEmpresa(empresaObject, categoria);
                    Log.d("Categoria", String.valueOf(itemEmpresa.getId()));
                    list.add(itemEmpresa);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
}
