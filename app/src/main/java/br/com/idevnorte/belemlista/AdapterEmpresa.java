package br.com.idevnorte.belemlista;


import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.idevnorte.belemlista.data.BelemContract;

/**
 * Adapter, responsavel por popular a view com o ItemEmpresa
 * <p>
 * Created by Miguel on 29/10/2017.
 */

public class AdapterEmpresa extends CursorAdapter {

    /**
     * Costrutor do Adapter
     *
     * @param context
     * @param cursor
     */
    public AdapterEmpresa(Context context,Cursor cursor) {
        super(context, cursor, 0);
    }


    /**
     * Metodo responsavel por guardar s informações do ItemEmpresa
     */
    public static class Holder {
        TextView titulo;
        ImageView icone;
        TextView tag;
        TextView endereco;
        TextView distancia;

        public Holder(View view) {
            this.titulo = view.findViewById(R.id.item_titulo);
            this.icone = view.findViewById(R.id.item_icone);
            this.tag = view.findViewById(R.id.item_tag);
            this.endereco = view.findViewById(R.id.item_endereco);
            this.distancia = view.findViewById(R.id.item_distancia);

        }
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int empresa = R.layout.item_empresa;
        View view = LayoutInflater.from(context).inflate(empresa, parent, false);

        Holder holder = new Holder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Holder holder = (Holder) view.getTag();
        int viewType = getItemViewType(cursor.getPosition());


        int tituloIndex = cursor.getColumnIndex(BelemContract.EmpresaEntry.COLUMN_EMPRESA);
        int iconeIndex = cursor.getColumnIndex(BelemContract.EmpresaEntry.COLUMN_ICONE);
        int enderecoIndex = cursor.getColumnIndex(BelemContract.EmpresaEntry.COLUMN_ENDERECO);
        int tagIndex = cursor.getColumnIndex(BelemContract.EmpresaEntry.COLUMN_TAG);

        holder.titulo.setText(cursor.getString(tituloIndex));
        holder.distancia.setText(null);
        holder.endereco.setText(cursor.getString(enderecoIndex));
        holder.tag.setText(cursor.getString(tagIndex));

        holder.icone.setImageResource(R.drawable.farmacia);
    }
}
