package br.com.idevnorte.belemlista.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Miguel on 18/11/2017.
 */

public class BelemProvider extends ContentProvider {

    private static final UriMatcher URI_MATCHER = buildUriMatcher();

    private BelemDBHelper dbHelper;

    private static final int CATEGORIA = 100;
    private static final int CATEGORIA_ID = 101;
    private static final int EMPRESA = 102;
    private static final int EMPRESA_ID = 103;


    private static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(BelemContract.CONTENT_AUTHORITY, BelemContract.PATH_CATEGORIA, CATEGORIA);
        uriMatcher.addURI(BelemContract.CONTENT_AUTHORITY, BelemContract.PATH_CATEGORIA + "/#", CATEGORIA_ID);

        uriMatcher.addURI(BelemContract.CONTENT_AUTHORITY, BelemContract.PATH_EMPRESA, EMPRESA);
        uriMatcher.addURI(BelemContract.CONTENT_AUTHORITY, BelemContract.PATH_EMPRESA + "/#", EMPRESA_ID);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new BelemDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase readableDatabase = dbHelper.getReadableDatabase();

        Cursor cursor;

        switch (URI_MATCHER.match(uri)) {
            case CATEGORIA:
                cursor = readableDatabase.query(BelemContract.CategoriaEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case EMPRESA:
                cursor = readableDatabase.query(BelemContract.EmpresaEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case CATEGORIA_ID:
                selection = BelemContract.CategoriaEntry._ID + " =?";
                selectionArgs = new String[]{String.valueOf(BelemContract.CategoriaEntry.getIdFromUri(uri))};
                cursor = readableDatabase.query(BelemContract.CategoriaEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case EMPRESA_ID:
                selection = BelemContract.EmpresaEntry._ID + " =?";
                selectionArgs = new String[]{String.valueOf(BelemContract.EmpresaEntry.getIdFromUri(uri))};
                cursor = readableDatabase.query(BelemContract.EmpresaEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("URI não identificada " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case CATEGORIA:
                return BelemContract.CategoriaEntry.CONTENT_TYPE;
            case CATEGORIA_ID:
                return BelemContract.CategoriaEntry.CONTENT_ITEM_TYPE;
            case EMPRESA:
                return BelemContract.EmpresaEntry.CONTENT_TYPE;
            case EMPRESA_ID:
                return BelemContract.EmpresaEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("URI não identificada " + uri);

        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase writableDatabase = dbHelper.getWritableDatabase();
        long id;
        switch (URI_MATCHER.match(uri)) {
            case CATEGORIA:
                id = writableDatabase.insert(BelemContract.CategoriaEntry.TABLE_NAME, null, values);
                if (id == -1) return null;
                getContext().getContentResolver().notifyChange(uri, null);
                return BelemContract.CategoriaEntry.buildUriForCategoria(id);
            case EMPRESA:
                id = writableDatabase.insert(BelemContract.EmpresaEntry.TABLE_NAME, null, values);
                if (id == -1) return null;
                getContext().getContentResolver().notifyChange(uri, null);
                return BelemContract.EmpresaEntry.buildUriForEmpresa(id);
            default:
                throw new IllegalArgumentException("URI não identificada " + uri);

        }

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase writableDatabase = dbHelper.getWritableDatabase();

        int delete = 0;

        switch (URI_MATCHER.match(uri)) {
            case CATEGORIA:
                delete = writableDatabase.delete(BelemContract.CategoriaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case EMPRESA:
                delete = writableDatabase.delete(BelemContract.EmpresaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CATEGORIA_ID:
                selection = BelemContract.CategoriaEntry._ID + " =?";
                selectionArgs = new String[]{String.valueOf(BelemContract.CategoriaEntry.getIdFromUri(uri))};

                delete = writableDatabase.delete(BelemContract.CategoriaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case EMPRESA_ID:
                selection = BelemContract.EmpresaEntry._ID + " =?";
                selectionArgs = new String[]{String.valueOf(BelemContract.EmpresaEntry.getIdFromUri(uri))};

                delete = writableDatabase.delete(BelemContract.EmpresaEntry.TABLE_NAME, selection, selectionArgs);
                break;
        }
        if (delete != 0) getContext().getContentResolver().notifyChange(uri, null);

        return delete;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase writableDatabase = dbHelper.getWritableDatabase();
        int update = 0;
        switch (URI_MATCHER.match(uri)) {
            case CATEGORIA:

                update = writableDatabase.update(BelemContract.CategoriaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case EMPRESA:

                update = writableDatabase.update(BelemContract.EmpresaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CATEGORIA_ID:
                selection = BelemContract.CategoriaEntry._ID + " =?";
                selectionArgs = new String[]{String.valueOf(BelemContract.CategoriaEntry.getIdFromUri(uri))};


                update = writableDatabase.update(BelemContract.CategoriaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case EMPRESA_ID:
                selection = BelemContract.EmpresaEntry._ID + " =?";
                selectionArgs = new String[]{String.valueOf(BelemContract.EmpresaEntry.getIdFromUri(uri))};


                update = writableDatabase.update(BelemContract.EmpresaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
        }
        if (update != 0) getContext().getContentResolver().notifyChange(uri, null);
        return update;
    }
}
