package br.com.idevnorte.belemlista;


import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.idevnorte.belemlista.data.BelemContract;

/**
 * Adapter, responsavel por popular a view com o ItemCategoria
 * <p>
 * Created by Miguel on 24/10/2017.
 */

public class AdapterCategoria extends CursorAdapter {


    /**
     * Costrutor do Adapter
     *
     * @param context
     * @param cursor
     */
    public AdapterCategoria(Context context, Cursor cursor) {

        super(context, cursor, 0);

    }

    /**
     * Metodo responsavel por guardar s informações do ItemCategoria
     */
    public static class Holder {
        TextView titulo;
        ImageView icone;

        public Holder(View view) {
            this.titulo = view.findViewById(R.id.item_titulo);
            this.icone = view.findViewById(R.id.item_icone);
        }
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int categoria = R.layout.item_categoria;
        View view = LayoutInflater.from(context).inflate(categoria, parent, false);

        Holder holder = new Holder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Holder holder = (Holder) view.getTag();
        int viewType = getItemViewType(cursor.getPosition());


        int tituloIndex = cursor.getColumnIndex(BelemContract.CategoriaEntry.COLUMN_CATEGORIA);
        int iconeIndex = cursor.getColumnIndex(BelemContract.CategoriaEntry.COLUMN_ICONE);


        holder.titulo.setText(cursor.getString(tituloIndex));
        int drawableID = context.getResources().getIdentifier(cursor.getString(iconeIndex), "drawable", context.getPackageName());
        holder.icone.setImageResource(drawableID);
    }
}
