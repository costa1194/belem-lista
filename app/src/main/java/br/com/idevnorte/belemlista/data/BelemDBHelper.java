package br.com.idevnorte.belemlista.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Miguel on 12/11/2017.
 */

public class BelemDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static  final String DATABASE_NAME = "belem_lista.db";

    public BelemDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      final String TABELA_CATEGORIA ="CREATE TABLE " +BelemContract.CategoriaEntry.TABLE_NAME +"(" +
                BelemContract.CategoriaEntry._ID +              " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                BelemContract.CategoriaEntry.COLUMN_CATEGORIA + " TEXT NOT NULL, "       +
                BelemContract.CategoriaEntry.COLUMN_ICONE +     " TEXT NOT NULL "        +
                ");";

        final String TABELA_EMPRESA ="CREATE TABLE " +BelemContract.EmpresaEntry.TABLE_NAME +"(" +
                BelemContract.EmpresaEntry._ID +                    " INTEGER PRIMARY KEY AUTOINCREMENT, "             +
                BelemContract.EmpresaEntry.COLUMN_EMPRESA +         " TEXT NOT NULL, "       +
                BelemContract.EmpresaEntry.COLUMN_ENDERECO +        " TEXT NOT NULL, "       +
                BelemContract.EmpresaEntry.COLUMN_ICONE +           " TEXT NOT NULL, "       +
                BelemContract.EmpresaEntry.COLUMN_TAG +             " TEXT NOT NULL, "       +
                BelemContract.EmpresaEntry.COLUMN_CATEGORIA__ID +   " INTEGER NOT NULL "     +
                ");";


        db.execSQL(TABELA_CATEGORIA);
        db.execSQL(TABELA_EMPRESA);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop TABLE " + BelemContract.EmpresaEntry.TABLE_NAME);
        db.execSQL("drop TABLE " + BelemContract.CategoriaEntry.TABLE_NAME);
        onCreate(db);

    }
}
