package br.com.idevnorte.belemlista;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Item Categoria: recebe todas as informações da Categoria
 *
 * Created by Miguel on 25/10/2017.
 */

public class ItemEmpresa implements Serializable{

    private long id;

    private String icone;

    private String titulo;

    private String tag;

    private String endereco;

    private String distancia;

    private ItemCategoria categoria;

    /**
     * Constutor do ItemCategoria que vai receber as informções obrigatorias para se criar uma empresa
     *
     * @param id
     * @param titulo
     * @param endereco
     * @param icone
     * @param tag
     * @param categoria
     */
    public ItemEmpresa(Long id,String titulo, String endereco, String icone, String tag, ItemCategoria categoria ) {
        this.id = id;
        this.icone = icone;
        this.titulo = titulo;
        this.tag = tag;
        this.endereco = endereco;
        this.categoria = categoria;
    }
    /**
     * Construtor do ItemEmpresa que vai receber o arquivo Json da API.
     *
     * @param json : Objeto em formato Json que contem todos os parametros de uma Empresa
     *
     * @throws JSONException : Erro caso o arquivo Json não possa ser lido.
     */
    public ItemEmpresa(JSONObject json, ItemCategoria categoria) throws JSONException {
        this.id = json.getLong("id");
        this.icone = json.getString("Icone");
        this.titulo = json.getString("Empresa");
        this.endereco = json.getString("Endereco");
        this.tag = json.getString("Tags");
        this.categoria = categoria;
    }

    /** Getter e Setter */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public ItemCategoria getCategoria() {
        return categoria;
    }

    public void setCategoria(ItemCategoria categoria) {
        this.categoria = categoria;
    }
}
