package br.com.idevnorte.belemlista;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import br.com.idevnorte.belemlista.data.BelemContract;

/**
 * Class Responsavel por guardar as Treads do aplicativo.
 * <p>
 * Created by Miguel on 13/11/2017.
 */

public class Tread {

    InputStream input;
    Context context;

    /**
     * Construtor da Tread onde recebe o contexto da activity e carrega o arquivo Json.
     *
     * @param context
     */
    public Tread(Context context) {
        try {
            this.input = context.getAssets().open("lista.json");
            this.context = context;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo de Inicialização da CategoriaAsyncTasck.
     *
     * @param adapter
     */
    public void Categoria(AdapterCategoria adapter) {
        new CategoriaAsyncTasck(adapter, input, context).execute();
    }

    /**
     * Metodo de Inicialização da EmpresaAsyncTasck.
     *
     * @param adapter
     * @param categoriaUri : Categoria escolhida pelo visitante
     */
    public void Empresa(AdapterEmpresa adapter, Uri categoriaUri) {
        new EmpresaAsyncTasck(adapter, categoriaUri, input, context).execute();
    }

    /**
     * Tread respponsavel por popular ler o arquivo Json e sironizar as informações com o banco de dados.
     */
    private static class CategoriaAsyncTasck extends AsyncTask<Void, Void, ArrayList<ItemCategoria>> {

        AdapterCategoria adapter;
        InputStream input;
        Context context;

        /**
         * Construtor da CategoriaAsyncTasck que recebe todos os parametros nescessarios
         *
         * @param adapter
         * @param input
         * @param context
         */
        public CategoriaAsyncTasck(AdapterCategoria adapter, InputStream input, Context context) {

            this.adapter = adapter;
            this.input = input;
            this.context = context;

        }

        /**
         * Metodo de execução da tread ele ler o arquivo Json inserindo cada linha em uma lista de ItemCategoria
         *
         * @param voids
         * @return
         */
        @Override
        protected ArrayList<ItemCategoria> doInBackground(Void... voids) {
            BufferedReader reader = null;

            try {

                if (input == null) {
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(input));


                String linha;
                StringBuffer buffer = new StringBuffer();
                while ((linha = reader.readLine()) != null) {
                    buffer.append(linha);
                    buffer.append("\n");
                }

                ArrayList<ItemCategoria> list = JsonUtil.categoriaJson(buffer.toString());
                for (ItemCategoria categoria : list) {
                    Log.d("Lista", categoria.getTitulo());
                }
                return list;

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Metodo de Conclusão da Tread sicroniza o banco de dados e adiciona os valores na view.
         *
         * @param itemCategoria
         */
        @Override
        protected void onPostExecute(ArrayList<ItemCategoria> itemCategoria) {

            if (itemCategoria == null) return;

            for (ItemCategoria categoria : itemCategoria) {
                ContentValues values = new ContentValues();
                values.put(BelemContract.CategoriaEntry._ID, categoria.getId());
                values.put(BelemContract.CategoriaEntry.COLUMN_ICONE, categoria.getIcone());
                values.put(BelemContract.CategoriaEntry.COLUMN_CATEGORIA, categoria.getTitulo());


                int update = context.getContentResolver().update(BelemContract.CategoriaEntry.buildUriForCategoria(categoria.getId()), values, null, null);


                if (update == 0)
                    context.getContentResolver().insert(BelemContract.CategoriaEntry.CONTENT_URI, values);
            }
        }
    }

    /**
     * Tread respponsavel por popular ler o arquivo Json e sironizar as informações com o banco de dados.
     */
    private static class EmpresaAsyncTasck extends AsyncTask<Void, Void, ArrayList<ItemEmpresa>> {


        AdapterEmpresa adapter;
        Uri categoriaUri;
        InputStream input;
        Context context;
        ItemCategoria categoria;

        /**
         * Construtor da EmpresaAsyncTasck que recebe todos os parametros nescessarios
         *
         * @param adapter
         * @param categoriaUri : Categoria escolhida pelo visitante
         * @param input
         * @param context
         */
        public EmpresaAsyncTasck(AdapterEmpresa adapter, Uri categoriaUri, InputStream input, Context context) {

            this.adapter = adapter;
            this.categoriaUri = categoriaUri;
            this.input = input;
            this.context = context;
        }

        /**
         * Metodo de execução da tread ele ler o arquivo Json inserindo cada linha em uma lista de ItemEmpresa
         *
         * @param voids
         * @return
         */
        @Override
        protected ArrayList<ItemEmpresa> doInBackground(Void... voids) {
            BufferedReader reader = null;

            try {

                if (input == null) {
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(input));


                String linha;
                StringBuffer buffer = new StringBuffer();
                while ((linha = reader.readLine()) != null) {
                    buffer.append(linha);
                    buffer.append("\n");
                }

                String[] projection = {
                        BelemContract.CategoriaEntry._ID,
                        BelemContract.CategoriaEntry.COLUMN_CATEGORIA,
                        BelemContract.CategoriaEntry.COLUMN_ICONE
                };
                String selection = BelemContract.CategoriaEntry._ID + " =?";
                String[] selectionArgs = new String[]{String.valueOf(1)};

                Log.d("Testando a Uri", String.valueOf(BelemContract.CategoriaEntry.getIdFromUri(categoriaUri)));

                Cursor cursor = context.getContentResolver().query(categoriaUri, projection, selection, selectionArgs, null);

                Log.d("Testando o Cursor", String.valueOf(cursor));

                int idIndex = cursor.getColumnIndex(BelemContract.CategoriaEntry._ID);
                int tituloIndex = cursor.getColumnIndex(BelemContract.CategoriaEntry.COLUMN_CATEGORIA);
                int iconeIndex = cursor.getColumnIndex(BelemContract.CategoriaEntry.COLUMN_ICONE);

                categoria = new ItemCategoria(cursor.getLong(idIndex), cursor.getString(tituloIndex), cursor.getString(iconeIndex));


                ArrayList<ItemEmpresa> list = JsonUtil.empresaJson(buffer.toString(), categoria);
                for (ItemEmpresa empresa : list) {
                    Log.d("Lista", empresa.getTitulo());
                }
                return list;

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Metodo de Conclusão da Tread sicroniza o banco de dados e adiciona os valores na view.
         *
         * @param itemEmpresas
         */
        @Override
        protected void onPostExecute(ArrayList<ItemEmpresa> itemEmpresas) {

            if (itemEmpresas == null) return;

            for (ItemEmpresa empresa : itemEmpresas) {
                ContentValues values = new ContentValues();
                values.put(BelemContract.EmpresaEntry._ID, empresa.getId());
                values.put(BelemContract.EmpresaEntry.COLUMN_ICONE, empresa.getIcone());
                values.put(BelemContract.EmpresaEntry.COLUMN_ENDERECO, empresa.getEndereco());
                values.put(BelemContract.EmpresaEntry.COLUMN_EMPRESA, empresa.getTitulo());
                values.put(BelemContract.EmpresaEntry.COLUMN_CATEGORIA__ID, empresa.getCategoria().getId());
                values.put(BelemContract.EmpresaEntry.COLUMN_TAG, empresa.getTag());


                int update = context.getContentResolver().update(BelemContract.EmpresaEntry.buildUriForEmpresa(empresa.getId()), values, null, null);


                if (update == 0)
                    context.getContentResolver().insert(BelemContract.EmpresaEntry.CONTENT_URI, values);
            }
        }
    }

}

